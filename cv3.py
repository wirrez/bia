import math
import random

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
from matplotlib.animation import FuncAnimation


def func( thelist ):
	val = 0.0
	for i in range(len(thelist)):
		val += pow(thelist[i],2)
	return val
def func2( thelist ):
	val = 0.0
	for i in range(len(thelist)-1):
		val += (100*np.power( np.power(thelist[i],2) - thelist[i+1],2) + np.power(1 - thelist[i],2) )
	return val
def func3( thelist ):
	val = 0.0
	for i in range(len(thelist)):
		val += ((-1 * thelist[i]) * np.sin(np.sqrt(np.fabs(thelist[i]))))
	return val
def func4( thelist) :
	val1 = 0.0
	val2 = 1.0
	for i in range(len(thelist)):
		val1 += (np.power(thelist[i],2) / 4000)
	for i in range(len(thelist)):
		val2 *= np.cos(thelist[i]/np.sqrt(i+1))
	return val1 - val2 + 1

# SimulatedAnnealing ALGORITHM
# HILL CLIMB ALGORITHM
def grad_ascent(pos,function):
	# scalar multiple for grad-ascent
	scalar = -0.1
	# central difference method to calculate gradient of the function
	delta = 0.5
	result = []
	for position in pos:
		deltaPos = (function([position+delta,0]) - function([position-delta,0],)) / (2.0 * delta)
		result.append( position + (scalar * deltaPos) )
	result.append(0)
	return result
def hill_climb_move(initial,next_move,function,spaces):
	current = initial
	maxstuck = 3
	stuck = 0
	points = []

	while True:
		current[2] = round(function([current[0],current[1]]),2) # round for limit
		next = next_move([current[0],current[1]],function)
		for i in range(len(spaces)):
			if next[i]< spaces[0]:
				next[i] = spaces[0]
			if next[i] > spaces[1]:
				next[i] = spaces[1]
		next[2] = round(function([next[0],next[1]]),2) # round for limit
		if next[2] < current[2]:
			points.append(next)
			current = next
		else:
			if stuck < maxstuck:
				stuck += 1
				current[0] = next[0] + (random.uniform(0.1,0.5)*next[0])
				current[1] = next[1] + (random.uniform(0.1,0.5)*next[1])
				for i in range(len(spaces)):
					if current[i]< spaces[0]:
						current[i] = spaces[0]
					if current[i] > spaces[1]:
						current[i] = spaces[1]
				current[2] = function([current[0],current[1]])
			else:
				break;
	return points

def hill_climb(NumberOfIndividual,steps,function,space):
	dim = [None] * (len(space) + 1)
	result = []
	starting = []
	for i in range(NumberOfIndividual):
		for j in range(0,len(space)):
			dim[j] = round(random.uniform(space[0],space[1]),2)
		dim[len(space)] = function([dim[0],dim[1]])
		starting.append(dim.copy())
		#result.append(dim.copy())

	for init in starting:
		#plot.scatter(init[0],init[1],init[2])
		#print(init)
		points = hill_climb_move(init,grad_ascent,function,space)
		#init[len(space)] = function(dim)
		result += points

	return starting, result


def blind(steps, function, space):

	dim = [None] * (len(space)+1)
	result = []
	for i in range(0,steps):
		for j in range(0,len(space)):
			dim[j] = round(random.uniform(space[0],space[1]),2)
		dim[len(space)]=function([dim[0],dim[1]])
		result.append(dim.copy())
	resultArr = sorted(result,key=lambda x: x[2],reverse=False)
	result.append(resultArr[0]) # Add best result to end array
	return result

def animate(num):
	#plot.s
	title.set_text('3D Test, Frame={}'.format(num+1))
	point = f2[num]
	plot.scatter(point[0],point[1],point[2],zorder='890')
	print(point)

def generateAround(point,number,function,space):

	Npoint =  [None] * (len(space) +1)
	points = []
	for i in range(number):
		Npoint[0] = random.uniform(point[0]-3,point[0]+3)
		while(Npoint[0]>space[1] or Npoint[0]< space[0]):
			Npoint[0] = random.uniform(point[0]-3,point[0]+3)
		Npoint[1] = random.uniform(point[1]-3,point[0]+3)
		while (Npoint[1] > space[1] or Npoint[1] < space[0]):
			Npoint[1] = random.uniform(point[1] - 3, point[1] + 3)
		Npoint[2] = function([point[0],point[1]])
		points.append(Npoint)
	return points

def simulatedAnnealing(NumberOfIndividual,function,space):
	xBest = None
	x0 = None
	T = 2.0
	Tf = 0.005
	delta = 0.0
	r = 0.0
	alpha = 0.99

	dim = [None] * (len(space) +1)
	result = []
	starting = []

	for i in range(NumberOfIndividual):
		for j in range(0,len(space)):
			dim[j] = round( random.uniform(space[0],space[1]) ,2)
		dim[len(space)] = round( function([dim[0],dim[1]]) ,2)
		starting.append(dim.copy())

		if xBest is None:
			x0 = starting[i]
		xBest = x0

		while T > Tf:
			pointsAround = generateAround(x0,10,function,space)
			x = pointsAround[random.randint(0,len(pointsAround)-1)]
			x[2]= function([x[0],x[1]])
			delta = x[2] - x0[2]
			if delta < 0:
				x0 = x
				if x[2] < xBest[2]:
					xBest = x
				result.append(x)
			else:
				r = random.uniform(0,1)
				if( r < math.exp(-(delta/T))):
					x0 = x
					result.append(x)
			T*=alpha

		xBest[2] = function([xBest[0], xBest[1]])
		return xBest,result
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
task = "SimulatedAnnealing"
if __name__ == "__main__":
	if task == "Blind":
		fig = plt.figure()
		plot = fig.gca(projection='3d')

		# Make data.
		X = np.arange(-4, 4, 0.1)
		Y = np.arange(-4, 4, 0.1)
		X, Y = np.meshgrid(X, Y)
		Z = func([X, Y])

		plot.set_xlabel('x axis')
		plot.set_ylabel('y axis')
		plot.set_zlabel('z axis')
		steps = 50
		f2 = blind(steps, func, (-4, 4))

		surf = plot.plot_surface(X, Y, Z, cmap=cm.coolwarm, linewidth=0, antialiased=False, alpha=0.5)

		# Add a color bar which maps values to colors.
		fig.colorbar(surf, shrink=0.5, aspect=10)
		title = plot.set_title('3D')

		anim = FuncAnimation(fig,animate,interval=150,frames=len(f2),repeat=False)
		plot.scatter(f2[len(f2)-1][0], f2[len(f2)-1][1], f2[len(f2)-1][2], s=20, color='RED', marker='v', zorder='999')  # print xBest

		plt.show()
	elif task == "HillClimb":
		fig = plt.figure()
		plot = fig.gca(projection='3d')

		X = np.arange(-4, 4, 0.1)
		Y = np.arange(-4,4,0.1)
		X,Y = np.meshgrid(X,Y)
		Z = func([X,Y]) # 0 because of function call in param -1

		plot.set_xlabel('x label')
		plot.set_ylabel('y label')
		plot.set_zlabel('z axis')

		#Algorithm
		NumberOfIndividual = 2
		steps = 15
		lbX = -4.0
		ubX = 4.0
		lbY = -4.0
		ubY = 4.0
		starting_points, f2 = hill_climb(NumberOfIndividual,steps,func,(-4,4))
		for point in starting_points:
			plot.scatter(point[0],point[1],point[2],color='RED',marker='v',zorder='999')
		anim = FuncAnimation(fig, animate, interval=100, frames=len(f2), repeat=False)
		#for poi in f2:
		#	plot.scatter(poi[0],poi[1],poi[2])
		#End of Algorithm

		surf = plot.plot_surface(X, Y, Z, cmap=cm.coolwarm, linewidth=0, antialiased=False, alpha=0.5)
		fig.colorbar(surf, shrink=0.5, aspect=10)
		title = plot.set_title('3D')
		plt.show()

	elif task == "SimulatedAnnealing":
		fig = plt.figure()
		plot = fig.gca(projection='3d')

		X = np.arange(-4, 4, 0.1)
		Y = np.arange(-4,4,0.1)
		X,Y = np.meshgrid(X,Y)
		Z = func([X,Y,0]) # 0 because of function call in param -1

		plot.set_xlabel('x label')
		plot.set_ylabel('y label')
		plot.set_zlabel('z axis')

		#Algorithm
		NumberOfIndividual = 1
		xBest, f2 = simulatedAnnealing(NumberOfIndividual,func,(-4,4))
		anim = FuncAnimation(fig, animate, interval=100, frames=len(f2), repeat=False)
		plot.scatter(xBest[0],xBest[1],xBest[2],s=20,color='RED',marker='v',zorder='999') #print xBest
		#End of Algorithm

		surf = plot.plot_surface(X, Y, Z, cmap=cm.coolwarm, linewidth=0, antialiased=False, alpha=0.5)
		fig.colorbar(surf, shrink=0.5, aspect=10)
		title = plot.set_title('3D')
		plt.show()

