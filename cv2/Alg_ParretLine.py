import random
import math
import numpy as np
import threading
import matplotlib.pyplot as plt


class Alg_ParrentLine:
    def __init__(self, spaces, pop_size, generation_size, func1, func2 ):
        self.generation_size = generation_size
        self.pop_size = pop_size
        self.f1 = func1
        self.f2 = func2
        self.population = []
        self.sets = []
        self.spaces = spaces
        arr = [-2, -1, 0, 2, 4, 1]
        i = 0
        for i in range(0, pop_size):
            self.population.append(arr[i])
            i += 1
            #self.population.append(random.uniform(spaces[0], spaces[1]))


    def get_parent_rwga(self, new_population):

        vals = [val for val, _ in new_population]
        minimum = min(vals)
        maximum = max(vals)

        def fit(val):
            return (val - minimum) / (maximum - minimum + 0.000001)

        fitness = [(fit(cost), x) for cost, x in new_population]
        r = random.uniform(0, 1)
        cumsum = 0
        for prop, x in fitness:
            cumsum += prop
            if cumsum > r:
                return new_population[0][1], x

        return new_population[0][1], new_population[1][1]

    def crossover(self, a, b):
        return (a + b) / 2

    def mutate(self, val):
        return val + np.random.normal()

    def get_nondominanted_sorting(self):
        num_of_solution = []
        sets_of_dominant = []
        ranks = []
        output_front = []
        for i in range(0, self.pop_size):
            num_of_solution.append(0)
            ranks.append(-1)
            sets_of_dominant.append(set())
            output_front.append(set())

        for i in range(0, self.pop_size):
            for j in range(0, self.pop_size):
                if i == j:
                    continue
                if self.f1(self.population[i]) >= self.f1(self.population[j]) and self.f2(self.population[i]) >= self.f2(self.population[j]):
                    sets_of_dominant[i].add(j) # add dominant index
                elif self.f1(self.population[j]) >= self.f1(self.population[i]) and self.f2(self.population[j]) >= self.f2(self.population[i]):
                    num_of_solution[i] += 1
            if num_of_solution[i] == 0:
                ranks[i] = 1
                output_front[0].add(i)

        i = 0
        while len(output_front[i]) > 0 and i + 1 < self.pop_size:
            Q = set()
            for p in output_front[i]:
                for q in sets_of_dominant[p]:
                    num_of_solution[q] -= 1
                    if num_of_solution[q] == 0:
                        ranks[q] += 1
                        Q.add(q)

            i += 1
            output_front[i] = Q

        return output_front

    def calculateDiversity(self, sets):
        distances = []
        for i in range(0, len(sets)):
            distances.append(0)

        for function in [self.f1, self.f2]:
            sets = sorted(sets, key=function)
            if len(sets) > 1:
                distances[0] = distances[len(sets)-1] = math.inf
            for i in range(1, len(sets) - 1):
                distances[i] += (function(sets[i+1]) - function(sets[i-1])) # ??????  fmax - f min
        # replace ID to number in population
        #for i in range(0, len(sets)):
        #    sets[i] = self.population[i]

        return sorted(zip(distances, sets))

    def evolution(self, new_population):
        evoluted = []
        for i in range(0, self.pop_size):
            p1, p2 = self.get_parent_rwga(new_population)
            child = self.mutate(self.crossover(p1, p2))
            evoluted.append(child)
        return evoluted

    def calcuateBetterY(self, pointX):
        xs = [i for i in pointX]
        ys = []

        for x in xs:
            if self.f1(x) > self.f2(x):
                ys.append(self.f1(x))
            else:
                ys.append(self.f2(x))

        plt.scatter(xs, ys)

    def run(self):
        for gen in range(0, self.generation_size):
            new_population = []
            self.sets = self.get_nondominanted_sorting()
            i = 0

            while len(new_population) + len(self.sets[i]) < self.pop_size:
                calculatevalues = self.calculateDiversity(self.sets[i])
                for val in calculatevalues:
                    new_population.append((val[0], self.population[val[1]]))
                i += 1

            self.population = np.array(list([x for _, x in new_population]) + self.evolution(new_population))
            self.pop_size = len(self.population)
            print(self.population)

            t = np.arange(self.spaces[0], self.spaces[1], 1)

            plt.clf()
            plt.subplot(2, 1, 1)
            plt.plot(t, self.f1(t))
            plt.plot(t, self.f2(t))
            for p in self.sets:
                self.calcuateBetterY([self.population[i] for i in p])

            plt.subplot(2, 1, 2)
            plt.xlim([-1, 3])
            plt.ylim([-4, 1])
            plt.plot(t, self.f1(t))
            plt.plot(t, self.f2(t))
            for p in self.sets:
                self.calcuateBetterY([self.population[i] for i in p])


        plt.show()



















